﻿function openNav() {
    $("#mySidenav").css('width', '250px');
    $("#jsMainContent").css('margin-left', '250px');
}

function closeNav() {
    $("#mySidenav").css('width', '0');
    $("#jsMainContent").css('margin-left', '0');
}